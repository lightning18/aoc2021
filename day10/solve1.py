
opening = ['(', '[', '{', '<']
closing = [')', ']', '}', '>']
scoring = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

with open('./input.txt') as f:
    data = f.read()

answer = 0
for line in data.split('\n'):
    stack = []
    for c in line:
        # print(c)
        if c in opening:
            stack.append(c)
        else:
            match = stack.pop()
            expected = closing[opening.index(match)]
            goods = match == opening[closing.index(c)]
            if goods:
                continue
            else:
                answer += scoring[c]
            # assert match == opening[closing.index(c)]

print('Answer: ' + str(answer))
