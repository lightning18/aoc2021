
opening = ['(', '[', '{', '<']
closing = [')', ']', '}', '>']
scoring = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
}

with open('./input.txt') as f:
    data = f.read()

answers = []
for line in data.split('\n'):
    stack = []
    bad = False
    answer = 0
    for c in line:
        # print(c)
        if c in opening:
            stack.append(c)
        else:
            match = stack.pop()
            expected = closing[opening.index(match)]
            goods = match == opening[closing.index(c)]
            if goods:
                continue
            else:
                # answer += scoring[c]
                bad = True

    if not bad:
        for r in stack[::-1]:
            answer = answer * 5
            answer += scoring[closing[opening.index(r)]]
        answers.append(answer)

answers = sorted(answers)
print('Answer: ' + str(answers[len(answers) // 2]))
