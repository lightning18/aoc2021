
with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
data = [[int(z) for z in c] for c in data]

max_x = len(data[0])
max_y = len(data)


def get_adjacent(x, y):
    ret = []
    if x > 0 and y > 0:
        ret.append((x -1, y - 1))
    if x > 0:
        ret.append((x - 1, y))
        if y < max_y - 1:
            ret.append((x - 1, y + 1))
    if y > 0:
        ret.append((x, y - 1))
        if x < max_x - 1:
            ret.append((x + 1, y - 1))
    if x < max_x - 1:
        ret.append((x + 1, y))
    if y < max_y - 1:
        ret.append((x, y + 1))
    if y < max_y - 1 and x < max_x - 1:
        ret.append((x + 1, y + 1))
    return ret


steps = 0
flash_count = 0
flash_stack = []

answer = 0


while sum([sum(m) for m in data]) > 0:
    for yy in range(max_y):
        for xx in range(max_x):
            new_value = data[yy][xx] + 1

            if new_value == 10:
                new_value = 0
                flash_stack.append((xx, yy))

            data[yy][xx] = new_value
    while len(flash_stack) > 0:
        flashing = flash_stack.pop()
        flash_count += 1
        adjacent_points = get_adjacent(flashing[0], flashing[1])
        for ap in adjacent_points:
            if data[ap[1]][ap[0]] == 0:
                continue
            new_value = data[ap[1]][ap[0]] + 1
            if new_value == 10:
                new_value = 0
                flash_stack.append((ap[0], ap[1]))
            data[ap[1]][ap[0]] = new_value
    steps += 1

answer = steps
print('Answer: ', str(answer))
