
with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
data = [[int(z) for z in c] for c in data]

max_x = len(data[0])
max_y = len(data)


def get_adjacent(x, y):
    points = [
        (x - 1, y - 1),
        (x - 1, y),
        (x - 1, y + 1),
        (x, y - 1),
        (x, y + 1),
        (x + 1, y - 1),
        (x + 1, y),
        (x + 1, y + 1),
    ]
    ret = []
    for p in points:
        if 0 <= p[0] < max_x and 0 <= p[1] < max_y:
            ret.append(p)

    return ret


steps = 100
flash_count = 0
flash_stack = []

answer = 0


while steps > 0:
    for yy in range(max_y):
        for xx in range(max_x):
            new_value = data[yy][xx] + 1

            if new_value == 10:
                new_value = 0
                flash_stack.append((xx, yy))

            data[yy][xx] = new_value
    while len(flash_stack) > 0:
        flashing = flash_stack.pop()
        flash_count += 1
        adjacent_points = get_adjacent(flashing[0], flashing[1])
        for ap in adjacent_points:
            if data[ap[1]][ap[0]] == 0:
                continue
            new_value = data[ap[1]][ap[0]] + 1
            if new_value == 10:
                new_value = 0
                flash_stack.append((ap[0], ap[1]))
            data[ap[1]][ap[0]] = new_value
    steps -= 1

answer = flash_count
print('Answer: ', str(answer))
