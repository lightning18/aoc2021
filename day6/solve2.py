with open('./input.txt') as f:
    data = f.read()

tracker = {i: 0 for i in range(9)}

for i in [int(n) for n in data.split(',')]:
    tracker[i] += 1

for _ in range(256):
    tmp = {i: 0 for i in range(9)}
    for i in tracker:
        if i == 0:
            tmp[8] += tracker[0]
            tmp[6] += tracker[0]
        else:
            tmp[i - 1] += tracker[i]

    tracker = tmp

print(sum(tracker.values()))
