with open('./input.txt') as f:
    data = f.read()

coords = []
folds = []

max_x = 0
max_y = 0

for line in data.split('\n'):
    if line.startswith('fold along y='):
        folds.append((0, int(line[line.index('=')+1:])))
    elif line.startswith('fold along x='):
        folds.append((int(line[line.index('=') + 1:]), 0))
    elif ',' in line:
        pair = line.split(',')
        coords.append((int(pair[0]), int(pair[1])))
        max_x = max(max_x, int(pair[0]))
        max_y = max(max_y, int(pair[1]))
    else:
        pass

grid = [[False for x in range(max_x + 1)] for y in range(max_y + 1)]
for p in coords:
    grid[p[1]][p[0]] = True


def fold_x(bool_grid, along):
    part1 = [x[:along] for x in bool_grid]
    part2 = [x[along+1:] for x in bool_grid]
    ret = []
    for yi, row in enumerate(part1):
        ret.append([r or part2[yi][along - xi - 1] for xi, r in enumerate(row)])
    return ret


def fold_y(bool_grid, along):
    # Get first
    part1 = bool_grid[:along]
    # Get second
    part2 = bool_grid[along+1:]
    if len(part2) != len(part1):
        part2.append([False for _ in range(len(part1[0]))])
    ret = []
    for i in range(along):
        ret.append([x or part2[along - i - 1][xi] for xi, x in enumerate(part1[i])])
    return ret


ff = 0
fold_count = len(folds)
while fold_count > 0:
    if folds[ff][0] == 0:
        grid = fold_y(grid, folds[ff][1])
    else:
        grid = fold_x(grid, folds[ff][0])
    ff += 1
    fold_count -= 1


# grid = fold_y(grid, folds[0][1])
total = 0
for yy in grid:
    for xx in yy:
        total += 1 if xx else 0

for line in grid:
    # For part 2
    print(''.join(['#' if l else '.' for l in line]))

answer = total
print('Answer: ' + str(answer))
