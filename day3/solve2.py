import numpy as np

with open('./input.txt') as f:
    data = f.read()

gamma = []

for line in data.split('\n'):
    gamma.append(list(line.strip()))

gamma_np = np.array(gamma)


def extract_int_from_single_arr(arr: np.ndarray):
    arr_bin = ''.join(arr[0])
    return int(arr_bin, 2)


# Most common
def find_oxygen_gen_rating_from_gamma(bit, gamma_ndarray: np.ndarray):
    transposed_gamma = np.transpose(gamma_ndarray)
    unique, counts = np.unique(transposed_gamma[bit], return_counts=True)
    filter_criteria = counts.argmax()
    filter_out = []
    for arr in gamma_ndarray:
        filter_out.append(int(arr[bit]) == filter_criteria)
    filtered = gamma_ndarray[filter_out]

    if len(gamma_ndarray) == 2:
        # Consider the 1 of current bit
        last_filter_out = []
        for arr in gamma_ndarray:
            last_filter_out.append(int(arr[bit]) == 1)
        return extract_int_from_single_arr(gamma_ndarray[last_filter_out])
    elif len(gamma_ndarray) == 1:
        # Found it!
        return extract_int_from_single_arr(gamma_ndarray)

    # Recurse
    return find_oxygen_gen_rating_from_gamma(bit + 1, filtered)


# Least common
def find_co2_scrubber_rating_from_gamma(bit, gamma_ndarray: np.ndarray):
    transposed_gamma = np.transpose(gamma_ndarray)
    unique, counts = np.unique(transposed_gamma[bit], return_counts=True)
    filter_criteria = counts.argmin()
    filter_out = []
    for arr in gamma_ndarray:
        filter_out.append(int(arr[bit]) == filter_criteria)
    filtered = gamma_ndarray[filter_out]

    if len(gamma_ndarray) == 2:
        # Consider the 0 of current bit
        last_filter_out = []
        for arr in gamma_ndarray:
            last_filter_out.append(int(arr[bit]) == 0)
        return extract_int_from_single_arr(gamma_ndarray[last_filter_out])
    elif len(gamma_ndarray) == 1:
        # Found it!
        return extract_int_from_single_arr(gamma_ndarray)

    # Recurse
    return find_co2_scrubber_rating_from_gamma(bit + 1, filtered)


oxygen_gen_rating = find_oxygen_gen_rating_from_gamma(0, gamma_np)
co2_scrubber_rating = find_co2_scrubber_rating_from_gamma(0, gamma_np)

print(oxygen_gen_rating)
print(co2_scrubber_rating)
print(oxygen_gen_rating * co2_scrubber_rating)
