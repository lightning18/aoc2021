import numpy as np

with open('./input.txt') as f:
    data = f.read()

gamma = []

for line in data.split('\n'):
    gamma.append(list(line.strip()))

gamma_np = np.array(gamma)
transposed = np.transpose(gamma_np)

gamma_rate = []
epsilon_rate = []

for row in transposed:
    unique, counts = np.unique(row, return_counts=True)

    gamma_rate.append(counts.argmax())
    epsilon_rate.append(counts.argmin())

    # unique_counts = dict(zip(unique, counts))
    # print(unique_counts)

gamma_rate_bin = ''.join(str(x) for x in gamma_rate)
gamma_rate_int = int(gamma_rate_bin, 2)
epsilon_rate_bin = ''.join(str(x) for x in epsilon_rate)
epsilon_rate_int = int(epsilon_rate_bin, 2)

print(gamma_rate_int * epsilon_rate_int)
