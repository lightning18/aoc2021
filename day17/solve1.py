from sys import maxsize

with open('./input.txt') as f:
    data = f.read()

boundary_x = sorted([int(i) for i in data[data.index('x=')+2:data.index(',')].split('..')])
boundary_y = sorted([int(i) for i in data[data.index('y=')+2:].split('..')])


def solve_v(t, iv):
    if abs(iv[0]) > t and iv[0] < 0:
        # vx = iv[0] + t
        raise NotImplemented
    elif iv[0] > t and iv[0] > 0:
        vx = iv[0] - t
    else:
        vx = 0

    vy = iv[1] - t
    # print((vx, vy))
    return vx, vy


def solve_xy(iv):
    t = 0
    x = 0
    y = 0
    while True:
        vt = solve_v(t, iv)
        x += vt[0]
        y += vt[1]
        yield x, y
        t += 1


possible_ivs = set()
best_y = -maxsize
max_search = max([*boundary_x, *boundary_y]) + 1

for gy in range(-max_search, max_search, 1):
    for gx in range(max_search):
        max_y = -maxsize
        for xy in solve_xy((gx, gy)):
            max_y = max(max_y, xy[1])
            if boundary_x[0] <= xy[0] <= boundary_x[1] and boundary_y[0] <= xy[1] <= boundary_y[1]:
                possible_ivs.add((gx, gy))
                best_y = max(best_y, max_y)
                # print('Current best:', best_y)
                break
            if xy[0] > boundary_x[1] or xy[1] < boundary_y[0]:
                break

# for xy in solve_xy((7, 2)):
#     print(xy)
#     if xy[1] < -100:
#         break

# print(solve_xy(7, (7, 2)))
# print(solve_xy(9, (7, 2)))
# answer = best_y
answer = len(possible_ivs)
print('Answer:', str(answer))
