
with open('./input.txt') as f:
    data = f.read()


positions = [int(p) for p in data.split(',')]
# sorted_positions = sorted(positions)
# weighted_ave = sum([p * count(sorted_positions, p, len(sorted_positions)) for p in sorted_positions])
# weighted_ave = weighted_ave / len(positions)

# max_min_avg = (max(positions) + min(positions)) // 2

# average = sum(positions) / len(positions)
# print('Average: ', str(average // 2))
# ideal_position = average // 2
# ideal_position = weighted_ave
# print('Ideal position: ', str(ideal_position))
# best = max(positions)
# try_position = best

# movements = [abs(p - ideal_position) for p in positions]
# print(sum(movements))

best = 9999999999
for i in range(max(positions)):
    answer = sum([abs(p - i) for p in positions])
    best = min(best, answer)

print('Answer: ', str(best))
