increasedCount = 0
prev = None
with open('./input.txt') as f:
    for line in f:
        current = int(line.strip())
        if prev is None:
            prev = current
            continue
        if current > prev:
            increasedCount += 1
        prev = current

print(increasedCount)
