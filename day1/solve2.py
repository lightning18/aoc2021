from itertools import tee

def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def triplewise(iterable):
    "Return overlapping triplets from an iterable"
    # triplewise('ABCDEFG') -> ABC BCD CDE DEF EFG
    for (a, _), (b, c) in pairwise(pairwise(iterable)):
        yield a, b, c

increasedCount = 0
prev = None

with open('./input.txt') as f:
    for chunk in triplewise(f):
        current = sum(int(str(s).strip()) for s in chunk)
        if prev ==  None:
            prev = current
            continue
        if current > prev:
            increasedCount += 1
        prev = current

print(increasedCount)
