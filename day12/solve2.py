from collections import Counter


def get_other(seg: tuple, one):
    return seg[0] if seg.index(one) == 1 else seg[1]


def explore(starting_point, all_segments, visited_list=None):
    if visited_list is None:
        visited_list = []
    ret = []
    for s in all_segments:
        if starting_point in s:
            other = get_other(s, starting_point)
            can_visit_twice = True
            if other == 'start' and 'start' in visited_list:
                continue
            if other == 'end' and 'end' in visited_list:
                continue
            visit_count = visited_list.count(other)

            lower_count = 1
            c = Counter(visited_list)
            for k in c.keys():
                if k not in ['start', 'end'] and k.islower() and c[k] >= 2:
                    lower_count -= 1
                    if lower_count < 0:
                        break
            if lower_count < 0:
                can_visit_twice = False

            max_visit = 2 if can_visit_twice else 1
            if other.isupper() or visit_count < max_visit:
                ret.append(s)
    # return discoverables
    return ret


with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
segments = [s.split('-') for s in data]
segments = [(s[0], s[1]) for s in segments]

paths = []


def navigate(starting_point, segments_list, visited_list=None):
    if visited_list is None:
        visited_list = []
    to_explore = explore(starting_point, segments_list, visited_list)
    for x in to_explore:
        other = get_other(x, starting_point)
        if other == 'end':
            lower_count = 1
            c = Counter(visited_list)
            for k in c.keys():
                if k not in ['start', 'end'] and k.islower() and c[k] >= 2:
                    lower_count -= 1
                    if lower_count < 0:
                        break
            if lower_count >= 0:
                paths.append(','.join(visited_list + ['end']))
        navigate(other, segments_list, visited_list + [other])


navigate('start', segments, ['start'])

answer = len(paths)
# for p in paths:
#     print(p)
print('Answer: ' + str(answer))
