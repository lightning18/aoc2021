
def get_other(seg: tuple, one):
    return seg[0] if seg.index(one) == 1 else seg[1]


def explore(starting_point, all_segments, visited_list=None):
    if visited_list is None:
        visited_list = []
    ret = []
    for s in all_segments:
        if starting_point in s:
            other = get_other(s, starting_point)
            if other not in visited_list or other.isupper():
                ret.append(s)
    # return discoverables
    return ret


with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
segments = [s.split('-') for s in data]
segments = [(s[0], s[1]) for s in segments]

paths = []


def navigate(starting_point, segments_list, visited_list=None):
    if visited_list is None:
        visited_list = []
    to_explore = explore(starting_point, segments_list, visited_list)
    for x in to_explore:
        other = get_other(x, starting_point)
        if other == 'end':
            paths.append(','.join(visited_list + ['end']))
        navigate(other, segments_list, visited_list + [other])


navigate('start', segments, ['start'])

answer = len(paths)
# print(paths)
print('Answer: ' + str(answer))
