
with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
data = [[int(z) for z in c] for c in data]

max_x = len(data[0])
max_y = len(data)


def get_adjacent(x, y):
    ret = []
    if x > 0:
        ret.append((x - 1, y))
    if y > 0:
        ret.append((x, y - 1))
    if x < max_x - 1:
        ret.append((x + 1, y))
    if y < max_y - 1:
        ret.append((x, y + 1))
    return ret


answer = 0
for yy in range(max_y):
    for xx in range(max_x):
        adjacents = get_adjacent(xx, yy)
        values = [data[a[1]][a[0]] for a in adjacents]
        value = data[yy][xx]
        if min(value, *values) == value and value not in values:
            answer += value + 1
        # print(adjacents)

print('Answer: ', str(answer))
