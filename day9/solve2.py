
with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
data = [[int(z) for z in c] for c in data]

max_x = len(data[0])
max_y = len(data)


def get_adjacent_points(x, y):
    ret = []
    if x > 0:
        ret.append((x - 1, y))
    if y > 0:
        ret.append((x, y - 1))
    if x < max_x - 1:
        ret.append((x + 1, y))
    if y < max_y - 1:
        ret.append((x, y + 1))
    return ret


def find_flow(x, y, scanned=None):
    if scanned is None:
        scanned = []
    if (x, y) in scanned:
        return scanned
    scanned.append((x, y))
    points = get_adjacent_points(x, y)
    for ap in points:
        if int(data[ap[1]][ap[0]]) < 9:
            scanned = find_flow(ap[0], ap[1], scanned)
            if ap not in scanned:
                scanned.append(ap)
    return scanned


basins = []
for yy in range(max_y):
    for xx in range(max_x):
        adjacent_points = get_adjacent_points(xx, yy)
        values = [data[a[1]][a[0]] for a in adjacent_points]
        value = data[yy][xx]
        if min(value, *values) == value and value not in values:
            f = find_flow(xx, yy)

            basins.append(len(f))

largest = sorted(zip(basins), reverse=True)[:3]

answer = 1
for ll in largest:
    answer = answer * ll[0]
print('Answer: ', str(answer))
