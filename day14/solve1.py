from collections import Counter
from itertools import tee


def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
polymer = data[0]
rules = {x[0]: x[1] for x in [d.split(' -> ') for d in data[2:]]}

step = 0
max_steps = 10
while step < max_steps:
    p = 1
    for pair in pairwise(polymer):
        insert = rules[''.join(pair)]
        polymer = polymer[:p * 2 - 1] + insert + polymer[p * 2 - 1:]
        p += 1
    step += 1

c = Counter(polymer)
counts = c.values()

answer = max(counts) - min(counts)
print('Answer:', answer)
