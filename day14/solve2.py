from collections import Counter
from functools import cache


with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
polymer = data[0]
rules = {x[0]: x[1] for x in [d.split(' -> ') for d in data[2:]]}


@cache
def count_polymer(polymer_data: str, steps: int):
    counts = {}
    for i in range(len(polymer_data) - 1):
        new_pair = rules[polymer_data[i] + polymer_data[i+1]]
        counts[new_pair] = counts.get(new_pair, 0) + 1
        if steps == 1:
            continue
        else:
            for pp, count in count_polymer(polymer_data[i] + new_pair + polymer_data[i+1], steps - 1).items():
                counts[pp] = counts.get(pp, 0) + count
    # print('Step:', steps)
    return counts


pc = count_polymer(polymer, 40)
c = Counter(polymer)
for p, v in c.items():
    pc[p] = pc.get(p, 0) + v


answer = max(pc.values()) - min(pc.values())
print('Answer:', answer)
