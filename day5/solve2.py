
def get_points_for_pair(pair):
    x0, y0 = pair[0]
    x1, y1 = pair[1]
    delta_x = x1 - x0
    delta_y = y1 - y0

    if delta_x == 0:
        step = (0, -1 if delta_y < 0 else 1)
    elif delta_y == 0:
        step = (-1 if delta_x < 0 else 1, 0)
    elif abs(delta_x) == abs(delta_y):
        step = (-1 if delta_x < 0 else 1, -1 if delta_y < 0 else 1)
    else:
        raise NotImplementedError

    ret = [pair[0]]

    new_point = pair[0]
    while new_point != pair[1]:
        new_point = [new_point[0] + step[0], new_point[1] + step[1]]
        ret.append(new_point)

    return ret


with open('./input.txt') as f:
    data = f.read()

board_size = 1000
board = [[0 for x in range(board_size)] for y in range(board_size)]

for line in data.split('\n'):
    coords = [[int(ll) for ll in l.split(',')] for l in line.split(' -> ')]

    # if coords[0][0] != coords[1][0] and coords[0][1] != coords[1][1]:
    #     continue
    points = get_points_for_pair(coords)
    for point in points:
        point_x = point[0]
        point_y = point[1]
        # prev = board[point_y][point_x]
        board[point_y][point_x] += 1

# Find max
answer = 0
for board_line in board:
    for n in board_line:
        if n >= 2:
            answer += 1

print(answer)
