
def get_points_for_pair(pair):
    x0, y0 = pair[0]
    x1, y1 = pair[1]
    deltax = x1 - x0
    deltay = y1 - y0

    if deltax < 0 or deltay < 0:
        step = -1
    else:
        step = 1

    ret = []

    if y0 == y1:
        for x in range(x0, x1, step):
            # y is same
            ret.append((x, y0))

    if x0 == x1:
        for y in range(y0, y1, step):
            # x is same
            ret.append((x0, y))

    return [*ret, pair[1]]


with open('./input.txt') as f:
    data = f.read()

board_size = 1000
board = [[0 for x in range(board_size)] for y in range(board_size)]

for line in data.split('\n'):
    coords = [[int(ll) for ll in l.split(',')] for l in line.split(' -> ')]

    if coords[0][0] != coords[1][0] and coords[0][1] != coords[1][1]:
        continue
    points = get_points_for_pair(coords)
    # points_list = list(points)
    for point in points:
        point_x = point[0]
        point_y = point[1]
        # prev = board[point_y][point_x]
        board[point_y][point_x] += 1

# Find max
answer = 0
for board_line in board:
    for n in board_line:
        if n >= 2:
            answer += 1

print(answer)
