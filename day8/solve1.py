with open('./sample.txt') as f:
    data = f.read()

data = [x.split(' | ') for x in data.split('\n')]

answer = 0
for d in data:
    four_digit_code = d[1].split(' ')
    len_4d = [len(x) for x in four_digit_code]
    answer += len_4d.count(2)
    answer += len_4d.count(4)
    answer += len_4d.count(3)
    answer += len_4d.count(7)

print('Answer: ', str(answer))
