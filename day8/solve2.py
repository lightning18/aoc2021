from operator import xor

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']


def get_bits(code):
    bits = ''
    for letter in letters:
        bits += '1' if letter in code else '0'
    return bits


def intersect(bits_a, bits_b):
    bits = ''
    for i in range(len(bits_a)):
        bits += '1' if bits_a[i] == bits_b[i] == '1' else '0'
    return bits


def count(bits):
    c = 0
    for i in range(len(bits)):
        c += 1 if bits[i] == '1' else 0
    return c


def compare_or(bits_a, bits_b):
    ret = ''
    for i in range(len(bits_a)):
        ret += '1' if bool(int(bits_a[i])) or bool(int(bits_b[i])) else '0'
    return ret


def compare_and(bits_a, bits_b):
    ret = ''
    for i in range(len(bits_a)):
        ret += '1' if bool(int(bits_a[i])) and bool(int(bits_b[i])) else '0'
    return ret


def bit_check(bits, bit, value):
    return bits[bit] == value


def split_bits(bits):
    ret = []
    for i, b in enumerate(bits):
        if b == '1':
            ret.append('0' * i + '1' + '0' * (6 - i))
    return ret


def get_given_number(groups, digits):
    number = ''
    mapping = {i: None for i in range(10)}
    bitmap = {i: None for i in range(7)}

    # Get 7 4 3 2
    # First pass
    bit_group = [get_bits(g) for g in groups]
    for group in bit_group:
        bits_count = count(group)
        if bits_count == 7:
            mapping[8] = group
        elif bits_count == 4:
            mapping[4] = group
        elif bits_count == 3:
            mapping[7] = group
        elif bits_count == 2:
            mapping[1] = group
        else:
            pass
    # Intersect 7 4 3 2
    cf = mapping[8]
    for x in [4, 7, 1]:
        cf = intersect(cf, mapping[x])

    # Find 6-segment without cf
    for group in bit_group:
        bits_count = count(group)
        if bits_count == 6 and xor(*[compare_or(group, s) == '1' * 7 for s in split_bits(cf)]):
            # Compare with 8
            mapping[6] = group

    # zero from 6 is c
    bitmap[2] = mapping[6].index('0')
    # remaining from cf is f
    bitmap[5] = compare_and(mapping[6], cf).index('1')

    # Find 5-segment with cf
    for group in bit_group:
        bits_count = count(group)
        # c 1 and f 1
        if group not in mapping.values() and bits_count == 5 and bit_check(group, bitmap[2], '1') and bit_check(group, bitmap[5], '1'):
            mapping[3] = group
            break

    # Find 5-segment without c
    for group in bit_group:
        bits_count = count(group)
        # c 0 and f 1
        if group not in mapping.values() and bits_count == 5 and bit_check(group, bitmap[2], '0') and bit_check(group, bitmap[5], '1'):
            mapping[5] = group
            group = group[0:bitmap[2]] + '1' + group[bitmap[2]+1:]
            bitmap[4] = group.index('0')

    # Find remaining 5-segment (with e)
    for group in bit_group:
        bits_count = count(group)
        if group not in mapping.values() and bits_count == 5:
            mapping[2] = group
            group = group[0:bitmap[5]] + '1' + group[bitmap[5]+1:]
            bitmap[1] = group.index('0')

    # Find 6-segment without e
    for group in bit_group:
        bits_count = count(group)
        # e 0
        if group not in mapping.values() and bits_count == 6 and bit_check(group, bitmap[4], '0'):
            mapping[9] = group

    # Find remaining 6-segment
    for group in bit_group:
        bits_count = count(group)
        if group not in mapping.values() and bits_count == 6:
            mapping[0] = group

    for digit in digits:
        bits = get_bits(digit)
        number += str(list(mapping.values()).index(bits))

    return int(number)


with open('./input.txt') as f:
    data = f.read()

data = [x.split(' | ') for x in data.split('\n')]

answer = 0
for d in data:
    four_digit_code = d[1].split(' ')

    answer += get_given_number(d[0].split(' '), d[1].split(' '))

print('Answer: ', str(answer))
