import operator
from functools import reduce

with open('./input.txt') as f:
    data = f.read()


class Packet:
    def __init__(self, version, packet_type, value=None, parent=None):
        self.version = version
        self.packet_type = packet_type
        self.value = value
        self.parent = parent

        self.bit_pos = -1
        self.multiple = False
        self.is_operator = False
        self.sub_packets: [Packet] = []

    @staticmethod
    def parse(bitdata, only_first=False):
        keep_going = True
        pos = 0
        count = 0
        multiple = False
        ret_packets = []
        while keep_going:
            if count > 0:
                multiple = True
            else:
                keep_going = not only_first

            version_raw = bitdata[pos:pos+3]
            if version_raw in ['']:
                break
            version = int(version_raw, 2)
            pos += 3

            packet_type_raw = bitdata[pos:pos+3]
            if packet_type_raw == '':
                break
            packet_type = int(packet_type_raw, 2)
            pos += 3

            packet = Packet(version, packet_type)

            if packet_type == 4:
                # Literal packet
                value_raw = ''
                while bitdata[pos] == '1':
                    value_raw += bitdata[pos+1:pos+5]
                    pos += 5
                # Get last part (zero-prefixed)
                value_raw += bitdata[pos+1:pos+5]
                packet_value = int(value_raw, 2)
                packet.value = packet_value
                pos += 5
            else:
                # Operator packet
                packet.is_operator = True

                length_type_id_raw = bitdata[pos:pos+1]
                if length_type_id_raw == '':
                    break
                length_type_id = int(length_type_id_raw, 2)
                pos += 1
                if length_type_id == 0:
                    sub_packets_length_raw = bitdata[pos:pos+15]
                    sub_packets_length = int(sub_packets_length_raw, 2)
                    pos += 15

                    sps = Packet.parse(bitdata[pos:pos+sub_packets_length])
                    for sp in sps:
                        packet.sub_packets.append(sp)
                    pos += sub_packets_length
                elif length_type_id == 1:
                    sub_packets_count_raw = bitdata[pos:pos+11]
                    sub_packets_count = int(sub_packets_count_raw, 2)
                    pos += 11
                    for i in range(sub_packets_count):
                        sps = Packet.parse(bitdata[pos:], only_first=True)
                        for sp in sps:
                            packet.sub_packets.append(sp)
                        pos += sps[-1].bit_pos
                else:
                    raise NotImplemented

            count += 1
            packet.bit_pos = pos
            packet.multiple = multiple
            ret_packets.append(packet)

        return ret_packets

    def version(self):
        return self.version

    def get_position(self):
        return self.bit_pos

    def add_sub_packet(self, sp):
        self.sub_packets.append(sp)

    def get_child_packets_versions(self):
        ret = []
        for cp in self.sub_packets:
            ret.append(cp.version)
            ret += cp.get_child_packets_versions()
        return ret

    def evaluate(self):
        if self.packet_type == 4:
            return self.value

        if self.packet_type == 0:
            operands = [sp.evaluate() for sp in self.sub_packets]
            if len(operands) == 1:
                return operands[0]
            return sum(operands)
        elif self.packet_type == 1:
            operands = [sp.evaluate() for sp in self.sub_packets]
            if len(operands) == 1:
                return operands[0]
            return reduce(operator.mul, operands)
            # return operator.mul(*operands)
        elif self.packet_type == 2:
            operands = [sp.evaluate() for sp in self.sub_packets]
            return min(operands)
        elif self.packet_type == 3:
            operands = [sp.evaluate() for sp in self.sub_packets]
            return max(operands)
        elif self.packet_type == 5:
            operands = [sp.evaluate() for sp in self.sub_packets]
            return 1 if operands[0] > operands[1] else 0
        elif self.packet_type == 6:
            operands = [sp.evaluate() for sp in self.sub_packets]
            return 1 if operands[0] < operands[1] else 0
        elif self.packet_type == 7:
            operands = [sp.evaluate() for sp in self.sub_packets]
            return 1 if operands[0] == operands[1] else 0
        else:
            raise NotImplemented


d = ''
for h in data.split('\n')[0]:
    d += "{0:04b}".format(int(h, 16))


ps = Packet.parse(d)
pp = ps[0]
# p = Packet(d)
# p = Packet('00101010000')
# print(p.packet_versions)
# print(p.get_child_packets_versions())

# packet_version_sum = p.version + sum(p.get_child_packets_versions())
# packet_version_sum = p.version + sum(p.sub_packet_versions)

answer = pp.evaluate()
# answer = 0
print('Answer:', answer)
