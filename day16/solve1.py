
with open('./input.txt') as f:
    data = f.read()


class Packet:
    def __init__(self, bitdata, parent=None, only_first=False):
        self.multiple = False
        self.packet_versions = []
        self.sub_packets = []
        self.sub_packet_versions = []
        self.bit_data = bitdata
        self.bit_pos = 0
        self.parent = parent
        p_keep_going = True
        p_count = 0
        while p_keep_going:
            if p_count > 0:
                self.multiple = True
            else:
                p_keep_going = not only_first

            # Get version
            p_version_raw = self.bit_data[self.bit_pos:self.bit_pos + 3]
            p_version = int(p_version_raw, 2)
            if not self.multiple:
                self.version = p_version
            self.packet_versions += [p_version]
            self.bit_pos += 3

            # Get type
            p_type_raw = self.bit_data[self.bit_pos:self.bit_pos + 3]
            p_type = int(p_type_raw, 2)
            self.bit_pos += 3

            if p_type == 4:
                # Get literal
                p_value_raw = ''
                while self.bit_data[self.bit_pos] == '1':
                    p_value_raw += self.bit_data[self.bit_pos + 1:self.bit_pos + 5]
                    self.bit_pos += 5
                # Get last part (zero-prefixed)
                p_value_raw += self.bit_data[self.bit_pos + 1:self.bit_pos + 5]
                p_value = int(p_value_raw, 2)
                self.bit_pos += 5
            else:
                # Operator packet
                p_length_type_id_raw = self.bit_data[self.bit_pos:self.bit_pos + 1]
                p_length_type_id = int(p_length_type_id_raw, 2)
                self.bit_pos += 1
                if p_length_type_id == 0:
                    p_sub_packets_length_raw = self.bit_data[self.bit_pos:self.bit_pos + 15]
                    p_sub_packets_length = int(p_sub_packets_length_raw, 2)
                    self.bit_pos += 15
                    # Sub packets here
                    sp = Packet(self.bit_data[self.bit_pos:self.bit_pos + p_sub_packets_length], parent=self)
                    self.sub_packets.append(sp)
                    self.sub_packet_versions.append(sp.version)
                    print(sp.packet_versions)
                    self.bit_pos += p_sub_packets_length
                    # p_keep_going = False
                elif p_length_type_id == 1:
                    p_sub_packets_count_raw = self.bit_data[self.bit_pos:self.bit_pos + 11]
                    p_sub_packets_count = int(p_sub_packets_count_raw, 2)
                    self.bit_pos += 11
                    # Read n sub packets
                    for i in range(p_sub_packets_count):
                        sp = Packet(self.bit_data[self.bit_pos:], parent=self, only_first=True)
                        self.sub_packets.append(sp)
                        self.sub_packet_versions.append(sp.version)
                        print(sp.packet_versions)
                        self.bit_pos += sp.bit_pos
                    # p_keep_going = False
                else:
                    print('UNKNOWN!')

            if len(self.bit_data) - self.bit_pos <= 6:
                p_keep_going = False

    def version(self):
        return self.version

    def get_position(self):
        return self.bit_pos

    def get_data(self):
        return self.bit_data

    def get_child_packets_versions(self):
        ret = []
        for cp in self.sub_packets:
            ret += cp.packet_versions
            ret += cp.get_child_packets_versions()
        return ret


d = ''
for h in data.split('\n')[0]:
    d += "{0:04b}".format(int(h, 16))


p = Packet(d)
# p = Packet('00101010000')
# print(p.packet_versions)
print(p.get_child_packets_versions())
packet_version_sum = p.version + sum(p.get_child_packets_versions())
# packet_version_sum = p.version + sum(p.sub_packet_versions)

# bit_pos = 0
# keep_going = True
# while keep_going:
#     # Get version
#     version_raw = d[bit_pos:bit_pos+3]
#     version = int(version_raw, 2)
#     packet_version_sum += version
#     bit_pos += 3
#
#     # Get type
#     ptype_raw = d[bit_pos:bit_pos+3]
#     ptype = int(ptype_raw, 2)
#     bit_pos += 3
#
#     if ptype == 4:
#         # Get literal
#         value_raw = ''
#         while d[bit_pos] == '1':
#             value_raw += d[bit_pos+1:bit_pos+5]
#             bit_pos += 5
#         # Get last part (zero-prefixed)
#         value_raw += d[bit_pos+1:bit_pos+5]
#         value = int(value_raw, 2)
#         bit_pos += 5
#     else:
#         # Operator packet
#         length_type_id_raw = d[bit_pos:bit_pos+1]
#         length_type_id = int(length_type_id_raw, 2)
#         bit_pos += 1
#         if length_type_id == 0:
#             sub_packets_length_raw = d[bit_pos:bit_pos+15]
#             sub_packets_length = int(sub_packets_length_raw, 2)
#             bit_pos += 15
#             # Sub packets here
#             p = Packet(d[bit_pos:bit_pos+sub_packets_length])
#             packet_version_sum += sum(p.packet_versions)
#             bit_pos += sub_packets_length
#         elif length_type_id == 1:
#             sub_packets_length_raw = d[bit_pos:bit_pos + 11]
#             sub_packets_length = int(sub_packets_length_raw, 2)
#             bit_pos += 11
#             for i in range(sub_packets_length):
#                 # packet_version_sum += sum(p.packet_versions)
#                 p = Packet(d[bit_pos:bit_pos+11], literal=True)
#                 packet_version_sum += sum(p.packet_versions)
#                 bit_pos += 11
#         else:
#             print('UNKNOWN!')
#
#     if len(d) - bit_pos <= 6:
#         keep_going = False


answer = packet_version_sum
print('Answer:', answer)
