from operator import add

# h-pos, depth
position = (0, 0)


def move_forward(current, units):
    return tuple(map(add, current, (units, 0)))


def move_backward(current, units):
    return tuple(map(add, current, (0 - units, 0)))


def move_up(current, units):
    return tuple(map(add, current, (0, 0 - units)))


def move_down(current, units):
    return tuple(map(add, current, (0, units)))


with open('./input1.txt') as f:
    for line in f:
        movement = line.strip().split(' ')
        direction = movement[0]
        velocity = int(movement[1])
        if direction == 'forward':
            position = move_forward(position, velocity)
        elif direction == 'backward':
            position = move_backward(position, velocity)
        elif direction == 'up':
            position = move_up(position, velocity)
        elif direction == 'down':
            position = move_down(position, velocity)
        else:
            print('Unknown direction')

print(position)
print(position[0] * position[1])
