with open('./sample.txt') as f:
    data = f.read()

data = data.split('\n')
data = [[int(z) for z in c] for c in data]

max_x = len(data[0])
max_y = len(data)

grid = [[0 for _ in range(max_x)] for _ in range(max_y)]
grid[0][0] = data[0][0]
for i in range(max_x - 1):
    grid[0][i + 1] = grid[0][i] + data[0][i + 1]
for i in range(max_y - 1):
    grid[i + 1][0] = grid[i][0] + data[i + 1][0]

for yy in range(max_y - 1):
    for xx in range(max_x - 1):
        grid[yy + 1][xx + 1] = min(grid[yy][xx + 1], grid[yy + 1][xx]) + data[yy + 1][xx + 1]

answer = grid[max_y - 1][max_x - 1] - grid[0][0]
print('Answer:', answer)
