# from functools import cache
from sys import maxsize

with open('./input.txt') as f:
    data = f.read()

data = data.split('\n')
data = [[int(z) for z in c] for c in data]

max_x = len(data[0])
max_y = len(data)

# Larger data
larger_data = [[0 for _ in range(max_x * 5)] for _ in range(max_y * 5)]
multiplier = 5
for iy in range(multiplier):
    for ix in range(multiplier):
        for yy in range(max_y):
            for xx in range(max_x):
                new_value = (data[yy][xx] + (ix + iy)) % 9
                larger_data[yy + (max_y * iy)][xx + (max_x * ix)] = new_value if not new_value == 0 else 9
data = larger_data
max_x = len(data[0])
max_y = len(data)


# @cache
def get_adjacent_points(point):
    ret = [
        (point[0], point[1] + 1),
        (point[0] + 1, point[1]),
        (point[0], point[1] - 1),
        (point[0] - 1, point[1]),
    ]
    return [p for p in ret if -1 < p[0] < max_x and -1 < p[1] < max_y]


visited = set()
distances = {(xx, yy): maxsize for yy in range(max_y) for xx in range(max_x)}
distances_size = len(distances)
# Set start point
distances[(0, 0)] = 0
pq = [((0, 0), 0)]

while len(pq) > 0:
    pq = sorted(pq, key=lambda kv: kv[1])
    q = pq.pop(0)

    vertex = q[0]
    value = q[1]
    visited.add(vertex)
    current = distances[vertex]
    if current < value:
        continue
    for ap in get_adjacent_points(vertex):
        new_distance = current + data[ap[1]][ap[0]]
        if ap not in visited and new_distance < distances[ap]:
            distances[ap] = current + data[ap[1]][ap[0]]
            pq.append((ap, distances[ap]))

# for yy in range(max_y):
#     for xx in range(max_x):
#         # visited += (xx, yy)
#         visited.add((xx, yy))
#         # visited.append((xx, yy))
#         print(len(visited) / distances_size)
#         aps = get_adjacent_points((xx, yy))
#         for ap in aps:
#             if ap not in visited:
#                 # Calculate distance
#                 current = distances[(xx, yy)]
#                 # Compare which to store
#                 distances[ap] = min(distances[ap], current + data[ap[1]][ap[0]])

answer = distances[(max_x - 1, max_y - 1)]
print('Answer:', answer)
