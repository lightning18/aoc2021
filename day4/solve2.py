
class Board:
    def __init__(self, numbers, idx):
        self.numbers = [ns.split() for ns in numbers]
        self.marked = [[False for x in range(5)] for y in range(5)]
        self.id = idx

    def has_won(self):
        transposed = list(map(list, zip(*self.marked)))
        for row in [*self.marked, *transposed]:
            if row == [True for x in range(5)]:
                return True
        return False

    def mark(self, number):
        marked = []
        for ns_idx, ns in enumerate(self.numbers):
            marked.append([True if n == ins else self.marked[ns_idx][idx] for idx, ins in enumerate(ns)])
        self.marked = marked

    def get_unmarked_sum(self):
        u_sum = 0
        for r_idx, row in enumerate(self.marked):
            for m_idx, m in enumerate(row):
                if not m:
                    u_sum += int(self.numbers[r_idx][m_idx])
        return u_sum


with open('./input.txt') as f:
    data = f.read()

split_data = data.split('\n')
numbers_called = split_data[0]

# init boards
boards = []
b_idx = 0
i = 1
while i < len(split_data) and split_data[i] == '':
    board = Board(split_data[i+1:i+6], b_idx)
    boards.append(board)
    i += 6
    b_idx += 1

boards_won = []

for n in numbers_called.split(','):
    for board in boards:
        # Mark
        board.mark(n)
        # Check if won
        if not board.has_won():
            continue

        if board.id not in boards_won:
            # Get unmarked numbers
            # Multiply with winning number
            print('Winning: ' + n)
            xsum = board.get_unmarked_sum()
            print('Unmarked sum: ' + str(xsum))
            answer = int(n) * xsum
            print('Answer: ' + str(answer))
            print('Board ID:', str(board.id))
        boards_won.append(board.id)

